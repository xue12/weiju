package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.dynamic.WjDynamic;
import com.huixi.microspur.web.mapper.WjDynamicMapper;
import com.huixi.microspur.web.service.WjDynamicService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjDynamicServiceImpl extends ServiceImpl<WjDynamicMapper, WjDynamic> implements WjDynamicService {

}
