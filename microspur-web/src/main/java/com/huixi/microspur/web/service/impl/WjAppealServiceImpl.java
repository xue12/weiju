package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.VO.ListPageAppealVO;
import com.huixi.microspur.web.entity.appeal.WjAppeal;
import com.huixi.microspur.web.mapper.WjAppealMapper;
import com.huixi.microspur.web.service.WjAppealService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {


    @Override
    public List<WjAppeal> listPageAppeal(ListPageAppealVO listPageAppealVO) {

        QueryWrapper<WjAppeal> queryWrapper = new QueryWrapper<>();
        // 点赞数排序
        if(listPageAppealVO.getEndorseCount()!=null){
            queryWrapper.orderByDesc("endorse_count");
        }
        // 评论数排序
        if(listPageAppealVO.getCommentCount()!=null){
            queryWrapper.orderByDesc("comment_count");
        }
        // 浏览量
        if(listPageAppealVO.getBrowseCount()!=null){
            queryWrapper.orderByDesc("browse_count");
        }
        // 创建时间 排序
        if(listPageAppealVO.getCreateTime()!=null){
            queryWrapper.orderByDesc("create_time");
        }

        IPage<WjAppeal> wjAppealIPage = new Page<>(listPageAppealVO.getPageNO(), listPageAppealVO.getPageSize());
        wjAppealIPage = page(wjAppealIPage, queryWrapper);
        List<WjAppeal> records = wjAppealIPage.getRecords();

        return records;
    }
}
