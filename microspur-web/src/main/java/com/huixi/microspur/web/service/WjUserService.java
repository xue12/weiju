package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.user.WjUser;
import com.huixi.microspur.web.entity.user.WxSesssionKeyDTO;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserService extends IService<WjUser> {


    /**
     *  获取session_key
     * @Author 李辉
     * @Date 2019/11/23 4:31
     * @param code 用户code
     * @return java.lang.String
     **/
    String getWxSessionKey(String code);


    /**
     * 获取用户加密信息 并且保存
     * @Author 叶秋
     * @Date 2020/2/5 13:44
     * @param encryptedData
     * @param session_key
     * @param ivKey
     * @return com.huixi.microspur.web.entity.user.WjUser
     **/
    WjUser getEncryptionUserInfo(String encryptedData, String session_key, String ivKey);



    /**
     *  根据用户code 获取用户的 openId 和 sessionKey , 来确定有没有授权过。
     * @Author 叶秋
     * @Date 2020/2/1 20:17
     * @param code 用户小程序的code
     * @return com.huixi.microspur.web.entity.user.WxSesssionKeyDTO
     **/
    WxSesssionKeyDTO detectionUserAuthorization(String code);



}
