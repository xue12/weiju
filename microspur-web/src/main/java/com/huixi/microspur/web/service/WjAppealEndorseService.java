package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.appeal.WjAppealEndorse;

/**
 *  诉求点赞记录表的 服务提供类
 * @Author 叶秋 
 * @Date 2020/3/15 15:26
 * @param 
 * @return 
 **/
public interface WjAppealEndorseService extends IService<WjAppealEndorse> {

    /**
     *  判断用户是否点赞
     * @Author 叶秋
     * @Date 2020/3/19 0:15
     * @param appealId 帖子id
     * @param userId 用户id
     * @return java.lang.Boolean
     **/
    Boolean isEndorse(String appealId, String userId);

    /**
     *  获取改诉求的总点赞数
     * @Author 叶秋
     * @Date 2020/3/19 0:25
     * @param appealId 帖子id
     * @return int
     **/
    int getTotleCount(String appealId);

    /**
     *  取消诉求点赞
     * @Author 叶秋
     * @Date 2020/3/20 0:37
     * @param wjAppealEndorse
     * @return java.lang.Boolean
     **/
    Boolean cancelEndorse(WjAppealEndorse wjAppealEndorse);

}
