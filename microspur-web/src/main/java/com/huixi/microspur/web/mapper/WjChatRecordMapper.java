package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.entity.chat.WjChatRecord;

/**
 * <p>
 * 聊天室-聊天记录 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatRecordMapper extends BaseMapper<WjChatRecord> {

}
