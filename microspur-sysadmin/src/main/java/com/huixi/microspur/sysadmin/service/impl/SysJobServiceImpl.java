package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.entity.SysJob;
import com.huixi.microspur.sysadmin.mapper.SysJobMapper;
import com.huixi.microspur.sysadmin.service.SysJobService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Service
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements SysJobService {

}
